FROM alpine:3.11

RUN apk add --update --no-cache bash=~5.0 curl=~7.79 jq=~1.6

COPY src /
COPY LICENSE pipe.yml README.md /
RUN wget --no-verbose -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh &&\
    chmod a+x /*.sh

ENTRYPOINT ["/release-blocker.sh"]