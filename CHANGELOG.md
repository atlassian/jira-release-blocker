# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.4

- patch: Internal maintenance: Move registry from atlassianlabs to bitbucketpipelines.

## 0.2.3

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.2.2

- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.
- patch: Removing release script complexity to reuse same functionality provided by bitbucket-pipe-release

## 0.2.1

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.2.0

- minor: Add support for encoded and non encoded JQL parameters - Jira Ticket: BLOCK-1

## 0.1.3

- patch: fix invalid syntax of pipe.yml

## 0.1.2

- patch: setup ssh keys for writing to master branch from pipelines build/deployment

## 0.1.1

- patch: OSR-475 rename bitbucket pipe to jira-release-blocker

## 0.1.0

- minor: Prepare for Open Source and Push Docker container to Atlassian Docker Hub organization user.

## 0.0.3

- patch: Add additional tests and fix curl command while also limiting response fields from jira via the fields query parameter greatly improving debug-ability.

## 0.0.2

- patch: Fix invalid environment variables for Jira Authentication.

## 0.0.1

- patch: Fix bitbucket pipelines to have initial release.
